<?php
/**
 * @file
 * openlayers_demo_map_feature.openlayers_layers.inc
 */

/**
 * Implements hook_openlayers_layers().
 */
function openlayers_demo_map_feature_openlayers_layers() {
  $export = array();

  $openlayers_layers = new stdClass;
  $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
  $openlayers_layers->api_version = 1;
  $openlayers_layers->name = 'openlayers_demo_mapbox_blue_marble_layer';
  $openlayers_layers->title = 'MapBox Blue Marble';
  $openlayers_layers->description = 'Demo TMS layer implementation with MapBox Blue Marble';
  $openlayers_layers->data = array(
    'base_url' => 'http://a.tiles.mapbox.com/v3/',
    'layername' => 'mapbox.blue-marble-topo-jul-bw',
    'baselayer' => 1,
    'type' => 'png',
    'resolutions' => array(
      0 => 156543.0339,
      1 => 78271.51695,
      2 => 39135.758475,
      3 => 19567.8792375,
      4 => 9783.93961875,
      5 => 4891.96980938,
      6 => 2445.98490469,
      7 => 1222.99245234,
      8 => 611.496226172,
    ),
    'wrapDateLine' => 0,
    'layer_type' => 'openlayers_layer_type_tms',
  );
  $export['openlayers_demo_mapbox_blue_marble_layer'] = $openlayers_layers;

  return $export;
}
